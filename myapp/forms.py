from django import forms
from myapp.models import customer
from django.contrib.auth.models import User

class userform(forms.ModelForm):
    
    class Meta():
        model=User
        fields = ('first_name','username','password')
        labels = {
            'first_name': 'Name',
            'username': 'Email',
        }
        widgets={
            'password':forms.PasswordInput()
        }

    
class profileform(forms.ModelForm):
    class Meta():
        model = customer
        fields = ('subtitle','profile_pic')
        